module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      build: {
        src: 'src/js/script.js',
        dest: 'build/js/script.min.js'
      }
    },
    cssmin: {
      build: {
        src: 'src/css/stylesheet.css',
        dest: 'build/css/stylesheet.min.css'
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin')

  // Default task(s).
  grunt.registerTask('default', ['uglify', 'cssmin']);

};
